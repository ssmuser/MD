#include "stdafx.h"

using namespace std;

// ���������� ����������
const int NumberElectrons = 20;

// ��� (LeapFrog)
double dt  = 1.0e-4; 
double dt_ = 1.0e-2;


// ����� ������������� ��������� - ���������� �������� ���������� - ���������������� ��������� ?   ���������? 
// ������������ ���������� �� ������ ������� �����
// => ������� �������� ����������������� ��������� (���������� ����� V, N, E)
// ��� ������ �����������, ��� ������ ���������� ������� (� ��� �������)

// ������������� �������
double E_pot(double x[], double y[], double z[])
{
	double function = 0;

	for (int i = 0; i < NumberElectrons; i++)
	{
		for (int j = i + 1; j < NumberElectrons; j++)
			function += 1 / sqrt(x[i] * x[i] + x[j] * x[j] - 2 * x[i] * x[j] * (sin(z[i]) * sin(z[j]) * cos(y[i] - y[j]) + cos(z[i]) * cos(z[j])));

		function += x[i] * x[i];
	}

	return function;
}

// ������������ �������
double E_kin(double vx[], double vy[], double vz[])
{
	double sum = 0;

	for (int i = 0; i < NumberElectrons; i++)
		sum += vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i];

	return 0.5 * sum;
}

// ������� ����������� �� x
double dx(double x[], double y[], double z[], int index)
{
	double plus[NumberElectrons], minus[NumberElectrons];

	memcpy(plus,  x, sizeof(double) * NumberElectrons);
	memcpy(minus, x, sizeof(double) * NumberElectrons);

	plus [index] += dt;
	minus[index] -= dt;

	return (E_pot(plus, y, z) - E_pot(minus, y, z)) / (2 * dt);
}

// ������� ����������� �� y
double dy(double x[], double y[], double z[], int index)
{
	double plus[NumberElectrons], minus[NumberElectrons];

	memcpy(plus,  y, sizeof(double) * NumberElectrons);
	memcpy(minus, y, sizeof(double) * NumberElectrons);

	plus [index] += dt;
	minus[index] -= dt;

	return (E_pot(x, plus, z) - E_pot(x, minus, z)) / (2 * dt);
}

// ������� ����������� �� z
double dz(double x[], double y[], double z[], int index)
{
	double plus[NumberElectrons], minus[NumberElectrons];

	memcpy(plus,  z, sizeof(double) * NumberElectrons);
	memcpy(minus, z, sizeof(double) * NumberElectrons);

	plus [index] += dt;
	minus[index] -= dt;

	return (E_pot(x, y, plus) - E_pot(x, y, minus)) / (2 * dt);
}

int main()
{
	setlocale(LC_ALL, "Russian");

	// ���������� ����������
	freopen("Output.txt", "w", stdout);	

	// ���������
	freopen("Input.txt", "r", stdin);


	// ���������� ������
	double x [NumberElectrons], y [NumberElectrons], z [NumberElectrons];
	// ��������
	double vx[NumberElectrons], vy[NumberElectrons], vz[NumberElectrons];
	// ���� 
	double Fx[NumberElectrons], Fy[NumberElectrons], Fz[NumberElectrons];


	// �������������
	std::random_device random_device;					// �������� ��������
	std::mt19937 generator(random_device());			// ��������� ��������� �����

	std::uniform_real_distribution <> po(0, 2);			// ����������� ������������� [0,   2]
	std::uniform_int_distribution  <> phi(0, 359);		// ����������� ������������� [0, 359]
	std::uniform_int_distribution  <> theta(0, 180);	// ����������� ������������� [0, 180]
	std::uniform_real_distribution <> vel(0, 1);		// ����������� ������������� [0,   1]
/*
	// ������� ���������
	for (int k = 0; k < NumberElectrons; k++)
	{
		x[k] = po(generator);
		y[k] = 3.141592653 / 180 * phi(generator);
		z[k] = 3.141592653 / 180 * theta(generator);
	}
*/
	// ��������� ���������� ������ �� �Input.txt� (�����������)
	string input;
	for (input; getline(cin, input);)
		if (input == ("|" + to_string(NumberElectrons) + "|"))
			for (int i = 0; i < NumberElectrons; i++)
			{
				getline(cin, input);
				istringstream iss(input);
				iss >> x[i] >> y[i] >> z[i];
			}
	// alpha
	double alpha = 0.1;

	for (int k = 0; k < NumberElectrons; k++)
		x[k] += vel(generator);


	// ������� ��������
	for (int i = 0; i < NumberElectrons; i++)
	{
		vx[i] = vel(generator); vy[i] = vel(generator); vz[i] = vel(generator);
	}
	
	for (int iterations = 100000; iterations > 0; iterations--)
	{
		// ������ ���
		for (int i = 0; i < NumberElectrons; i++)
		{
			Fx[i] = -dx(x, y, z, i); Fy[i] = -dy(x, y, z, i); Fz[i] = -dz(x, y, z, i);
		}

		// LeapFrog
		for (int i = 0; i < NumberElectrons; i++)
		{
			vx[i] += Fx[i] * dt_; vy[i] += Fy[i] * dt_; vz[i] += Fz[i] * dt_;

			x[i]  += vx[i] * dt_;  y[i] += vy[i] * dt_;  z[i] += vz[i] * dt_;
		}

			
		double EKIN = E_kin(vx, vy, vz); 
		double EPOT = E_pot(x, y, z);

	///	double T = 2. / 3. * EKIN / NumberElectrons;		// T = 3.4 ?			// T = 2 ?

		
		// ������������� ���������
		double answer = 0;
		for (int i = 0; i < NumberElectrons; i++)
			answer += sqrt(vx[i] * vx[i] + vy[i] * vy[i] + vz[i] * vz[i]);
		
		cout << EKIN << "\t" << EPOT << "\t" << EKIN + EPOT << "\t" << answer / NumberElectrons;		
		for (int i = 0; i < NumberElectrons; i++)
			cout <<  "\t" << x[i];
		cout << endl;
	}	
	
	return 0;
}

