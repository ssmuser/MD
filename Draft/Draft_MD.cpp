#include "stdafx.h"

using namespace std;

double max_phi = 3.141592653 / 180 * 359;

// Количество электронов
const int NumberElectrons = 20;

// Шаг
double dt  = 1.0e-6;	// Разностная производная
double dt_ = 1.0e-4;	// Leapfrog

// Среднеквадратичное отклонение
double sigma_r, sigma_r_in, sigma_r_out, sigma_r_7;
double sigma_p, sigma_p_in, sigma_p_out, sigma_p_7;
double E, T, answer;


// Максимальное количество итераций
int maxIter = 50000, maxIter_ = maxIter + 1;


// Потенциальная энергия
double E_pot(double x[], double y[])
{
	double function = 0;

	for (int i = 0; i < NumberElectrons; i++)
	{
		for (int j = i + 1; j < NumberElectrons; j++)
			//function += 1 / pow(sqrt(x[i] * x[i] + x[j] * x[j] - 2 * x[i] * x[j] * cos(y[i] - y[j])), 5);
			function += 1 / sqrt(x[i] * x[i] + x[j] * x[j] - 2 * x[i] * x[j] * cos(y[i] - y[j]));

		function += x[i] * x[i];
	}

	return function;
}

// Кинетическая энергия
double E_kin(double vx[], double vy[])
{
	double sum = 0;

	for (int i = 0; i < NumberElectrons; i++)
		sum += vx[i] * vx[i] + vy[i] * vy[i];

	return 0.5 * sum;
}

// Частная производная по x
double dx(double x[], double y[], int index)
{
	double plus[NumberElectrons], minus[NumberElectrons];

	memcpy(plus,  x, sizeof(double) * NumberElectrons);
	memcpy(minus, x, sizeof(double) * NumberElectrons);

	plus [index] += dt;
	minus[index] -= dt;

	return (E_pot(plus, y) - E_pot(minus, y)) / (2 * dt);
}

// Частная производная по y
double dy(double x[], double y[], int index)
{
	double plus[NumberElectrons], minus[NumberElectrons];

	memcpy(plus,  y, sizeof(double) * NumberElectrons);
	memcpy(minus, y, sizeof(double) * NumberElectrons);

	plus [index] += dt;
	minus[index] -= dt;

	return (E_pot(x, plus) - E_pot(x, minus)) / (2 * dt);
}

// Среднеквадратичное отклонение
double StandardDeviation(double a[], int index, int n)
{
	double mean = 0, variance = 0;

	for (int i = index; i < n; i++)
		mean += a[i];

	mean /= (n - index);

	for (int i = index; i < n; i++)
		variance += (a[i] * a[i] - mean * mean);

	variance /= (n - index);

	return sqrt(variance);
}

// Среднеквадратичное отклонение углов (ближайший в своей оболочке)
double StandardDeviationRadInner(double a[], int index, int n, double b[])
{
	double mean = 0, variance = 0;
	double near = 9999, delta, min;
	
	double _min[NumberElectrons];

	for (int i = index; i < n; i++)
	{
		for (int j = index; j < n; j++)
		{
			if (i != j)
			{
				delta = sqrt(b[i] * b[i] + b[j] * b[j] - 2 * b[i] * b[j] * cos(a[i] - a[j]));;

				if (delta < near)
				{
					near = delta;
					min = a[j];
				}
			}
		}

		_min[i] = min;
		near = 9999;
	}

	for (int i = index; i < n; i++)
		mean += a[i] - _min[i];

	mean /= (n - index);

	for (int i = index; i < n; i++)
		variance += (a[i] - _min[i]) * (a[i] - _min[i]) - mean * mean;

	variance /= (n - index);

	return sqrt(variance);
}

// Среднеквадратичное отклонение углов (ближайший в соседней оболочке)
double StandardDeviationRadOuter(double a[], int index, int n, double b[])
{
	double mean = 0, variance = 0;
	double near = 99980001, delta, min;

	double _min[NumberElectrons];

	for (int i = index; i < n; i++)
	{
		for (int j = n; j < NumberElectrons; j++)
		{
			if (i != j)
			{
				delta = b[i] * b[i] + b[j] * b[j] - 2 * b[i] * b[j] * cos(a[i] - a[j]);;

				if (delta < near)
				{
					near = delta;
					min = a[j];
				}
			}
		}

		_min[i] = min;
		near = 99980001;
	}

	for (int i = index; i < n; i++)
		mean += a[i] - _min[i];

	mean /= (n - index);

	for (int i = index; i < n; i++)
		variance += (a[i] - _min[i]) * (a[i] - _min[i]) - mean * mean;

	variance /= (n - index);

	return sqrt(variance);
}


int main()
{
	// Записываем результаты
	freopen("Output.txt", "w", stdout);	
	// Считываем
	freopen("Input.txt", "r", stdin);

	// Координаты частиц
	double x [NumberElectrons], y [NumberElectrons];
	// Скорость
	double vx[NumberElectrons], vy[NumberElectrons];
	// Сила 
	double Fx[NumberElectrons], Fy[NumberElectrons];

	// alpha - отклонение
	for (double alpha = 0; alpha < 0.15; alpha += 0.0005)
	{
		sigma_r = 0, sigma_r_in = 0, sigma_r_out = 0, sigma_r_7 = 0;
		sigma_p = 0, sigma_p_in = 0, sigma_p_out = 0, sigma_p_7 = 0;
		
		T = 0;
					
		// Считываем координаты частиц из «Input.txt» (сферические)
		string input;
		for (input; getline(cin, input);)
			if (input == ("|" + to_string(NumberElectrons) + "|"))
				for (int i = 0; i < NumberElectrons; i++)
				{
					getline(cin, input);
					istringstream iss(input);
					iss >> x[i] >> y[i];
				}


		// Сумма всех смещений должна быть = 0
		for (int i = 0; i < NumberElectrons; i++)
		{
			if (i % 2 == 0)
			{
				x[i] += alpha;
			//	y[i] += 180 / 3.141592653 * alpha;
			}
			else
			{
				x[i] -= alpha;
			//	y[i] -= 180 / 3.141592653 * alpha;
			}
		}

		// Задание скорости
		for (int i = 0; i < NumberElectrons; i++)
		{
			vx[i] = 0; vy[i] = 0;
		}

		for (int iterations = 65000; iterations > 0; iterations--)
		{
			// Расчет сил
			for (int i = 0; i < NumberElectrons; i++)
			{
				Fx[i] = -dx(x, y, i); Fy[i] = -dy(x, y, i);
			}

			// LeapFrog
			for (int i = 0; i < NumberElectrons; i++)
			{
				vx[i] += Fx[i] * dt_; vy[i] += Fy[i] * dt_;

				x[i] += vx[i] * dt_;  y[i] += vy[i] * dt_;
			}

			// Система в равновесии
			if (iterations < maxIter)
			{
				T += E_kin(vx, vy);

				// КАКАЯ-ТО ДИЧЬ
				double copyX[NumberElectrons], copyY[NumberElectrons];
				memcpy(copyY, y, sizeof(double) * NumberElectrons);
				
				for (int i = 0; i < NumberElectrons; i++)
				{
					copyX[i] = fabs(x[i]);

					while (copyY[i] > max_phi)
						copyY[i] -= max_phi;

					while (copyY[i] < 0)
						copyY[i] += max_phi;
				}
				
				// r 
				sigma_r += StandardDeviation(copyX, 0, NumberElectrons);				
				sigma_r_in += StandardDeviation(copyX, 0, 8);
				sigma_r_out += StandardDeviation(copyX, 8, NumberElectrons);
				sigma_r_7 += StandardDeviation(copyX, 1, 8);
				
				// Phi
				sigma_p += StandardDeviationRadInner(copyY, 8, NumberElectrons, copyX);
				sigma_p_in += StandardDeviationRadInner(copyY, 1, 8, copyX);
				sigma_p_out += StandardDeviationRadOuter(copyY, 1, 8, copyX);
				sigma_p_7 += StandardDeviationRadOuter(copyY, 0, 8, copyX);
			}
			else if (iterations == maxIter_)
				E = E_kin(vx, vy) + E_pot(x, y);

	
			//double EKIN = E_kin(vx, vy);
			//double EPOT = E_pot(x, y);

		/*
			// Распределение скоростей
			double answer = 0;
			for (int i = 0; i < NumberElectrons; i++)
				answer += sqrt(vx[i] * vx[i] + vy[i] * vy[i]);

			cout << EKIN << "\t" << EPOT << "\t" << EKIN + EPOT << "\t" << answer / NumberElectrons;

			for (int i = 0; i < NumberElectrons; i++)
				cout <<  "\t" << x[i];
			for (int i = 0; i < NumberElectrons; i++)
				cout << "\t" << y[i];
			cout << endl;
		*/			
		}
		
		cout << alpha << "\t" << E << "\t" << T / 3 * 2 / maxIter
			<< "\t" << sigma_r / maxIter << "\t" << sigma_r_in / maxIter
			<< "\t" << sigma_r_out / maxIter << "\t" << sigma_r_7 / maxIter
			<< "\t" << sigma_p / maxIter << "\t" << sigma_p_in / maxIter
			<< "\t" << sigma_p_out / maxIter << "\t" << sigma_p_7 / maxIter << endl;
			
	}
	return 0;
}

